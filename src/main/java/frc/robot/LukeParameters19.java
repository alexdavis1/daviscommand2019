/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class LukeParameters19 {
    // AUTONOMOUS
    public static final double AUTO_MIN_SPEED = 0.75; //in ft/sec
    public static final double AUTO_MAX_SPEED = 4.0; //in ft/sec

    public static final double AUTO_MOVE_DIVISOR = 0.5;
    public static final double AUTO_TURN_CORRECTION_DIVISOR = 15;

    public static final double AUTO_TURN_DIVISOR = 100.0;
    public static final double AUTO_TURN_THRESHOLD = 0.4; //in degrees

    public static final double AUTO_FRONT_TRACKING_SPEED = 2.0; //in ft/sec
    public static final double AUTO_BACK_TRACKING_SPEED = -2.0; //in ft/sec
    public static final double AUTO_LOADING_STATION_TARGET_SCORING_WIDTH = 200;
    public static final double AUTO_ROCKET_TARGET_SCORING_WIDTH = 200;

    public static final double AUTO_VISION_CORRECTION_DIVISOR = 80;

    // DRIVE SYSTEM: SPEED CONTROLLERS
    public static final int LEFT_TOP_DRIVE_CONTROLLER_CAN_ID = 0;
    public static final int LEFT_FRONT_DRIVE_CONTROLLER_CAN_ID = 1;
    public static final int LEFT_BACK_DRIVE_CONTROLLER_CAN_ID = 2;
    public static final int RIGHT_TOP_DRIVE_CONTROLLER_CAN_ID = 3;
    public static final int RIGHT_FRONT_DRIVE_CONTROLLER_CAN_ID = 4;
    public static final int RIGHT_BACK_DRIVE_CONTROLLER_CAN_ID = 5;

    // DRIVE SYSTEM: ENCODERS
    public static final int LEFT_DRIVE_ENCODER_CHANNEL_A = 2;
    public static final int LEFT_DRIVE_ENCODER_CHANNEL_B = 3;
    public static final boolean INVERT_LEFT_DRIVE_ENCODER = false;

    public static final int RIGHT_DRIVE_ENCODER_CHANNEL_A = 4;
    public static final int RIGHT_DRIVE_ENCODER_CHANNEL_B = 5;
    public static final boolean INVERT_RIGHT_DRIVE_ENCODER = true;

    public static final double DRIVE_ENCODER_PULSES_PER_REVOLUTION = 2048.0;

    // DRIVE SYSTEM: VELOCITY CONTROL
    public static final double MAX_DRIVE_SPEED = 17.0; //in ft/sec

    public static final double LOW_GEAR_PROPORTIONAL_COEFFICIENT = 0.1;
    public static final double LOW_GEAR_INTEGRAL_COEFFICIENT = 0.0;
    public static final double LOW_GEAR_DERIVATIVE_COEFFICIENT = 0.0;
    public static final double LOW_GEAR_FEEDFORWARD_TERM = 0.0;

    public static final double HIGH_GEAR_PROPORTIONAL_COEFFICIENT = 0.03;
    public static final double HIGH_GEAR_INTEGRAL_COEFFICIENT = 0.0;
    public static final double HIGH_GEAR_DERIVATIVE_COEFFICIENT = 0.0;
    public static final double HIGH_GEAR_FEEDFORWARD_TERM = 0.0;

    public static final double DRIVE_PID_TOLERANCE = 0.01;

    // DRIVE SYSTEM: INPUT MODIFIERS
    public static final double DRIVE_STICK_DEADBAND = 0.05;
    public static final double DRIVE_STICK_SHIFT_POINT = 0.5;

    public static final double LOW_GEAR_DRIVE_STICK_COEFFICIENT = 8.5;
    public static final double HIGH_GEAR_DRIVE_STICK_COEFFICIENT = 25.5;
    public static final double HIGH_GEAR_DRIVE_STICK_OFFSET = 8.5;

    public static final double DRIVE_WHEEL_DEADBAND = 0.1;
    public static final double DRIVE_WHEEL_COEFFICIENT = 0.5;

    // DRIVE SYSTEM: SHIFTERS
    public static final int SHIFTER_SOLENOID_PCM_CHANNEL = 0;

    public static final double DRIVE_SHIFT_UP_POINT = 4.5; // in ft/sec
    public static final double DRIVE_SHIFT_DOWN_POINT = 4.0; // in ft/sec

    // SENSORS
    public static final int IMU_CAN_ID = 0;

    // CONSTANTS
    public static final double WHEEL_RADIUS = 2.0; // in inches  
  // For example to map the left and right motors, you could define the
  // following variables to use with your drivetrain subsystem.
  // public static int leftMotor = 1;
  // public static int rightMotor = 2;

  // If you are using multiple modules, make sure to define both the port
  // number and the module. For example you with a rangefinder:
  // public static int rangefinderPort = 1;
  // public static int rangefinderModule = 1;
}
