/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.CounterBase;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.SpeedControllerGroup;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    //drivetrain
    private SpeedControllerGroup leftDrive;
    private Encoder leftEncoder;

    private SpeedControllerGroup rightDrive;
    private Encoder rightEncoder;

    private Solenoid shifterSolenoid;

    //lift
    private WPI_TalonSRX liftMotor;
    private Encoder liftEncoder;
    private Solenoid claw;

    public RobotMap(){
        //drivetrain
        double DRIVE_WHEEL_RADIUS = 2.0;
        double DRIVE_ENCODER_PULSES_PER_REVOLUTION = 2048.0;
        double DRIVE_SYSTEM_GEAR_RATIO = 1.0;

        var leftTopMotor = new WPI_VictorSPX(3);
        leftTopMotor.setInverted(true);
        var leftFrontMotor = new WPI_VictorSPX(1);
        var leftBackMotor = new WPI_VictorSPX(4);
        this.leftDrive = new SpeedControllerGroup(leftTopMotor , leftFrontMotor , leftBackMotor);
        leftDrive.setInverted(false);
        this.leftEncoder = new Encoder(7, 8 , false , CounterBase.EncodingType.k4X);
        leftEncoder.setDistancePerPulse((2 * (Math.PI) * (DRIVE_WHEEL_RADIUS / 12)) / (DRIVE_ENCODER_PULSES_PER_REVOLUTION * DRIVE_SYSTEM_GEAR_RATIO));

        var rightTopMotor = new WPI_VictorSPX(2);
        rightTopMotor.setInverted(true);
        var rightFrontMotor = new WPI_VictorSPX(0);
        var rightBackMotor = new WPI_VictorSPX(5);
        this.rightDrive = new SpeedControllerGroup(rightTopMotor , rightFrontMotor , rightBackMotor);
        rightDrive.setInverted(true);
        this.rightEncoder = new Encoder(4 , 5 , true , CounterBase.EncodingType.k4X);
        rightEncoder.setDistancePerPulse((2 * (Math.PI) * (DRIVE_WHEEL_RADIUS / 12)) / (DRIVE_ENCODER_PULSES_PER_REVOLUTION * DRIVE_SYSTEM_GEAR_RATIO));

        this.shifterSolenoid = new Solenoid(0);

        //Lift
        this.liftMotor = new WPI_TalonSRX(6);
        this.liftEncoder = new Encoder(1, 2 , false , CounterBase.EncodingType.k4X);
        
        //Claw
        this.claw = new Solenoid(1);


    }//end constructor

    public SpeedControllerGroup[] getDriveMotors(){
        SpeedControllerGroup[] driveMotors = {leftDrive , rightDrive};
        return driveMotors;
    }//end getDriveMotors

    public Encoder[] getDriveEncoders(){
        Encoder[] driveEncoders = {leftEncoder , rightEncoder};
        return driveEncoders;
    }

    public Solenoid getShifter(){
        return shifterSolenoid;
    }

    public WPI_TalonSRX getLiftMotor(){
        return liftMotor;
    }//end getLiftMotor

    public Encoder getLiftEncoder(){
        return liftEncoder;
    }//end getLiftEncoder

    public Solenoid getClaw(){
        return claw;
    }

  // For example to map the left and right motors, you could define the
  // following variables to use with your drivetrain subsystem.
  // public static int leftMotor = 1;
  // public static int rightMotor = 2;

  // If you are using multiple modules, make sure to define both the port
  // number and the module. For example you with a rangefinder:
  // public static int rangefinderPort = 1;
  // public static int rangefinderModule = 1;
}
