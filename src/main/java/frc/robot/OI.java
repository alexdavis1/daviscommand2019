/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
  private Joystick throttle;
  private Joystick steering;
  private XboxController mechControl;
  private Joystick prajBox;

  private JoystickButton lWheelPaddle;
  
  private JoystickButton xboxA;
  private JoystickButton xboxB;
  private JoystickButton xboxX;
  private JoystickButton xboxY;
  private JoystickButton xboxLB;
  private JoystickButton xboxRB;
  private JoystickButton xboxBack;
  private JoystickButton xboxStart;
  private JoystickButton xboxLeftClickStick;
  private JoystickButton xboxRightClickStick;

  private double xDeadBand;

  public OI(){
    this.throttle = new Joystick(0);
    this.steering = new Joystick(1);
    this.mechControl = new XboxController(2);
    this.prajBox = new Joystick(3);

    this.lWheelPaddle = new JoystickButton(steering, 5);
    
    this.xboxA = new JoystickButton(mechControl, 1);
    this.xboxB = new JoystickButton(mechControl, 2);
    this.xboxX = new JoystickButton(mechControl, 3);
    this.xboxY = new JoystickButton(mechControl, 4);
    this.xboxLB = new JoystickButton(mechControl, 5);
    this.xboxRB = new JoystickButton(mechControl, 6);
    this.xboxBack = new JoystickButton(mechControl, 7);
    this.xboxStart = new JoystickButton(mechControl, 8);
    this.xboxLeftClickStick = new JoystickButton(mechControl, 9);
    this.xboxRightClickStick = new JoystickButton(mechControl, 10);

    this.xDeadBand = .1;
    

  }//end constructor

  public double getThrottle(boolean isInverted){
    double throttleVal;
    if (isInverted){
      throttleVal = -throttle.getY();
    }//end if
    else{
      throttleVal = throttle.getY();
    }//end else
    return throttleVal;
  }//end getThrottle

  public Joystick getWheel(){
    return steering;
  }

  public double getSteering(boolean isInverted){
    double wheelVal;
    if (isInverted){
      wheelVal = -steering.getY();
    }//end if
    else{
      wheelVal = steering.getY();
    }//end else
    return wheelVal;
  }//end getSteering

  public JoystickButton getLWheelPaddle(){
    return lWheelPaddle;
  }


  //XBox getters
  //sticks
  public double getXLeftStickY(boolean isInverted){
    double leftStickYVal;
    if (mechControl.getY(Hand.kLeft) > xDeadBand || mechControl.getY(Hand.kLeft) < -xDeadBand){
      leftStickYVal = 0.0;
    }
    else if (isInverted){
      leftStickYVal = -mechControl.getY(Hand.kLeft);
    }//end if
    else{
      leftStickYVal = mechControl.getY(Hand.kLeft);
    }//end else
    return leftStickYVal;
  }

  //buttons
  public JoystickButton getXboxA(){
    return xboxA;
  }
  public JoystickButton getXboxB(){
    return xboxB;
  }
  public JoystickButton getXboxX(){
    return xboxX;
  }
  public JoystickButton getXboxY(){
    return xboxY;
  }
  public JoystickButton getXboxLB(){
    return xboxLB;
  }
  public JoystickButton getXboxRB(){
    return xboxRB;
  }
  public JoystickButton getXboxBack(){
    return xboxBack;
  }
  public JoystickButton getXboxStart(){
    return xboxStart;
  }
  public JoystickButton getXboxLeftClickStick(){
    return xboxLeftClickStick;
  }
  public JoystickButton getXboxRightClickStick(){
    return xboxRightClickStick;
  }
 
  //// CREATING BUTTONS
  // One type of button is a joystick button which is any button on a
  //// joystick.
  // You create one by telling it which joystick it's on and which button
  // number it is.
  // Joystick stick = new Joystick(port);
  // Button button = new JoystickButton(stick, buttonNumber);

  // There are a few additional built in buttons you can use. Additionally,
  // by subclassing Button you can create custom triggers and bind those to
  // commands the same as any other Button.

  //// TRIGGERING COMMANDS WITH BUTTONS
  // Once you have a button, it's trivial to bind it to a button in one of
  // three ways:

  // Start the command when the button is pressed and let it run the command
  // until it is finished as determined by it's isFinished method.
  // button.whenPressed(new ExampleCommand());

  // Run the command while the button is being held down and interrupt it once
  // the button is released.
  // button.whileHeld(new ExampleCommand());

  // Start the command when the button is released and let it run the command
  // until it is finished as determined by it's isFinished method.
  // button.whenReleased(new ExampleCommand());
}
