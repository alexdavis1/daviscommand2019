/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import com.ctre.phoenix.sensors.PigeonIMU;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import frc.robot.subsystems.Claw;
import frc.robot.commands.ClawCommand;
import frc.robot.commands.DriveCommand;
import frc.robot.commands.LiftCommand;
import frc.robot.commands.LiftHold;
import frc.robot.commands.LiftOverrideCommand;
import frc.robot.commands.EnableTipProtectionCommand;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Lift;

//import com.ctre.phoenix.sensors.PigeonIMU;


/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  public  OI oi;
  public RobotMap rMap;
  public DriveTrain driveTrain;
  public Lift lift;
  public frc.robot.subsystems.Claw claw;

  public Command m_autonomousCommand;
  public Command driveRobot;
  public Command liftOverride;
  public Command enableTipProtect;
  public boolean overrideInactive;

  public PigeonIMU imu;
  public double[] ypr;
 
  /**
   * This function is run when the robot is first started up and should be
   * used for any initialization code.
   */
  @Override
  public void robotInit() {
    this.oi = new OI();
    this.rMap = new RobotMap();
    this.driveTrain = new DriveTrain(oi , rMap.getDriveMotors() , rMap.getDriveEncoders() , rMap.getShifter() , imu);
    this.lift = new Lift(rMap.getLiftMotor(), rMap.getLiftEncoder());
    this.claw = new Claw(rMap.getClaw());

    this.imu = new PigeonIMU(0);
    this.ypr = new double[3];

    this.overrideInactive = true;

    lift.setDefaultCommand(new LiftHold(lift));
    driveTrain.setDefaultCommand(new DriveCommand(oi, driveTrain));
    this.enableTipProtect = new EnableTipProtectionCommand(oi , driveTrain)
   
  }

  /**
   * This function is called every robot packet, no matter the mode. Use
   * this for items like diagnostics that you want ran during disabled,
   * autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before
   * LiveWindow and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {
    imu.getYawPitchRoll(ypr);
  }

  /**
   * This function is called once each time the robot enters Disabled mode.
   * You can use it to reset any subsystem information you want to clear when
   * the robot is disabled.
   */
  @Override
  public void disabledInit() {
  }

  @Override
  public void disabledPeriodic() {
    Scheduler.getInstance().run();
  }

  /**
   * This autonomous (along with the chooser code above) shows how to select
   * between different autonomous modes using the dashboard. The sendable
   * chooser code works with the Java SmartDashboard. If you prefer the
   * LabVIEW Dashboard, remove all of the chooser code and uncomment the
   * getString code to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional commands to the
   * chooser code above (like the commented example) or additional comparisons
   * to the switch structure below with additional strings & commands.
   */
  @Override
  public void autonomousInit() {
  
    /*
     * String autoSelected = SmartDashboard.getString("Auto Selector",
     * "Default"); switch(autoSelected) { case "My Auto": autonomousCommand
     * = new MyAutoCommand(); break; case "Default Auto": default:
     * autonomousCommand = new ExampleCommand(); break; }
     */

    // schedule the autonomous command (example)
    if (m_autonomousCommand != null) {
      m_autonomousCommand.start();
    }
  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
    Scheduler.getInstance().run();
  }

  @Override
  public void teleopInit() {
    // This makes sure that the autonomous stops running when
    // teleop starts running. If you want the autonomous to
    // continue until interrupted by another command, remove
    // this line or comment it out.
    if (m_autonomousCommand != null) {
      m_autonomousCommand.cancel();
    }
    oi.getXboxY().whenPressed(new LiftCommand(lift, "scale"));
    oi.getXboxB().whenPressed(new LiftCommand(lift, "switch"));
    oi.getXboxA().whenPressed(new LiftCommand(lift, "bottom"));
    oi.getXboxLB().whenPressed(new ClawCommand(claw , true));//open
    oi.getXboxRB().whenPressed(new ClawCommand(claw , false));//close
    oi.getXboxLeftClickStick().whenPressed(new LiftOverrideCommand(oi , lift));

    oi.getLWheelPaddle().whenReleased(enableTipProtect);
    oi.getLWheelPaddle().cancelWhenPressed(enableTipProtect);
  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopPeriodic() {

    Scheduler.getInstance().run();
  }

  /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {
  }
}
