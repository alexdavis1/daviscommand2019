/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.commands.LiftHold;

/**
 * Add your docs here.
 */
public class Lift extends Subsystem {
  private WPI_TalonSRX liftMotor;
  private Encoder liftEncoder;
  private int distanceDeadBand;
  private int[] liftPosition;
  private int holdPosition;
  private int currPosition;
  private boolean finished;

  public Lift(WPI_TalonSRX liftMotor , Encoder liftEncoder){
    this.liftMotor = liftMotor;
    this.liftEncoder = liftEncoder;
    this.distanceDeadBand = 6000;
    this.liftPosition = new int[4];
    liftPosition[0] = 0;//bottom
    liftPosition[1] = 58000;//switch
    liftPosition[2] = 188000;//scale
    liftPosition[3] = 190000;//softstop
    this.holdPosition = 0;
    this.currPosition = 0;
    this.finished = true;
  }//end constructor

  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  public void setPosition(String position){
    if(position.equals("bottom")){
      holdPosition = liftPosition[0];
    }
    else if(position.equals("switch")){
      holdPosition = liftPosition[1];
    }
    else if(position.equals("scale")){
      holdPosition = liftPosition[2];
    }
  }//end setPosition

  public void manualOverride(double speed){
    currPosition = liftEncoder.getRaw();
    if ((speed > 0 && currPosition < liftPosition[3]) || (speed < 0 && currPosition > liftPosition[0])){
      liftMotor.set(speed);
    }
    else {
      liftMotor.stopMotor();
    }
  }//end manualOverride

  public void holdCurrentPosition(){
    this.holdPosition = liftEncoder.getRaw();
  }

  public void hold(){
    currPosition = liftEncoder.getRaw();
    if ((currPosition < (holdPosition - distanceDeadBand)) && (currPosition < liftPosition[3])){
      liftMotor.set((holdPosition - currPosition) / (liftPosition[3] / 2));
      finished = false;
    }//end too low
    else if (currPosition > (holdPosition + distanceDeadBand)&& (currPosition > liftPosition[0])){
      liftMotor.set(-(currPosition - holdPosition) / (liftPosition[0] / 2));
      finished = false;
    }//end too high
    else{
      liftMotor.stopMotor();
      finished = true;
    }//end just right
  }//end hold

  public boolean getLiftFinished(){
    return finished;
  }//end getFinished

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
    
  }
}
