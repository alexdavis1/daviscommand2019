/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.sensors.PigeonIMU;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import frc.robot.OI;
import frc.robot.commands.DriveCommand;
import frc.robot.commands.TipRecoveryCommand;

/**
 * Add your docs here...
 */
public class DriveTrain extends Subsystem {
  private DifferentialDrive drive;
  private Encoder[] driveEncoder;
  private Solenoid shifter;
  private boolean inLowGear;
  private OI oi;

  public PigeonIMU imu;
  public double[] ypr;

  private double highGearThreshold;
  private double lowGearThreshold;

  private Command tipRecover;
  private boolean isTipping;

  public DriveTrain(OI oi , SpeedControllerGroup[] driveMotors , Encoder[] driveEncoder , Solenoid shifter , PigeonIMU imu){
    this.drive = new DifferentialDrive(driveMotors[0], driveMotors[1]);
    this.driveEncoder = driveEncoder;//left 0 , right 1
    this.shifter = shifter;
    this.inLowGear = false;
    this.oi = oi;
    this.imu = imu;

    this.tipRecover = new TipRecoveryCommand();
    this.isTipping = false;

    this.highGearThreshold = 4;
    this.lowGearThreshold = 2.5;
  }
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  public void driveRobot(double y , double x){
    if ((driveEncoder[0].getRate() > highGearThreshold || driveEncoder[1].getRate() > highGearThreshold) && inLowGear){
      shifter.set(false);
      inLowGear = false;
    }
    else if ((driveEncoder[0].getRate() < lowGearThreshold) || (driveEncoder[1].getRate() < lowGearThreshold) && !inLowGear){
      shifter.set(true);
      inLowGear = true;
    }
    drive.arcadeDrive(y, x);
  }//end driveRobot

  public void tipDetection(){
    imu.getYawPitchRoll(ypr);
    if (Math.abs(ypr[1]) >= 10) { //Activate tipping protection
      isTipping = true;
      tipRecover.start();
    }
    else if (Math.abs(ypr[1]) < 1 && isTipping) {
      isTipping = false;
    }
  }

  public boolean getIsTipping(){
    return isTipping;
  }

  @Override
  public void initDefaultCommand() {
    
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
  }
}
